// 1.Функції  уникають необхідності писати один і той самий код знову, роблячи його  коротшим і легшим для читання.
// 2. Надати функції дані для введення, щоб вона могла виконати своє завдання, використовуючи ці дані. 
// 3.Оператор return використовується всередині функції, щоб вказати значення, яке функція поверне при її виконані.


function calculate(num1, num2, operator) {
    let result;
    switch(operator) {
      case '+':
        result = num1 + num2;
        break;
      case '-':
        result = num1 - num2;
        break;
      case '*':
        result = num1 * num2;
        break;
      case '/':
        result = num1 / num2;
        break;
      default:
        result = 'Invalid operator';
    }
    return result;
  }
  let num1 = parseInt(prompt('First number:'));
  let num2 = parseInt(prompt('Second number:'));
  let operator = prompt('Enter operator (+, -, *, /):');
  let result = calculate(num1, num2, operator);
  console.log(result);
  